Scriptname VehicleLightsScript extends ObjectReference

;-- Properties --------------------------------------
float Property LightsOffTime = 7.0 Auto
{ The time at which lights should be turned off }
float Property LightsOnTime = 18.0 Auto
{ The time at which lights should be turned on }
bool Property bNightActivated = True Auto Const
{ If bNightActivated to true, power will be enabled at night. Reverse if set to false. }

;-- Variables ---------------------------------------

;-- Functions ---------------------------------------

float Function GetCurrentHourOfDay() global
{ Returns the current time of day in hours since midnight }
	float Time = Utility.GetCurrentGameTime()
	Time -= Math.Floor(Time) as float
	Time *= 24 as float
	return Time
EndFunction

Event OnTimer(int aiTimerID)
	Self.CheckIfStateChangeNeeded()
EndEvent

Function CheckIfStateChangeNeeded()
	Self.StartTimer(10, 0)
	float fCurrentTime = VehicleLightsScript.GetCurrentHourOfDay()
	If (fCurrentTime > LightsOffTime && fCurrentTime < LightsOnTime)
		Self.GoToState("DayTimeState")
	Else
		Self.GoToState("NightTimeState")
	EndIf
EndFunction

Event OnInit()
	Self.CheckIfStateChangeNeeded()
EndEvent

;-- State -------------------------------------------
State DayTimeState

	Event OnBeginState(string sOldState)
		If (bNightActivated)
			Self.Disable()
		Else
			Self.Enable()
		EndIf
	EndEvent
EndState

;-- State -------------------------------------------
State NightTimeState

	Event OnBeginState(string sOldState)
		If (bNightActivated)
			Self.Enable()
		Else
			Self.Disable()
		EndIf
	EndEvent
EndState