Scriptname AttachVisorOverlayScript extends ObjectReference  
{Attaches a camera attach fx based on the property set in the reference.}
;===============================================

import game				; game.psc for access to getPlayer()

VisualEffect Property CameraAttachFX Auto 
{Particle art to attach to camera}
Keyword Property VisorKeyword Auto

; Events
;===============================================

Event OnEquipped(Actor akActor)
	If (akActor == getPlayer() as ObjectReference)
		;If (GetPlayer().WornHasKeyword(VisorKeyword))
		CameraAttachFX.Play(GetPlayer())
		;Else
		;CameraAttachFX.Stop(GetPlayer())
		;Endif
	Endif
	
EndEvent

Event OnUnequipped(Actor akActor)
	If (akActor == getPlayer() as ObjectReference)
		CameraAttachFX.Stop(GetPlayer())
	Endif
EndEvent
